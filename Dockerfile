FROM nwlucas/ubuntu
MAINTAINER Nigel Williams-Lucas

ENV KIBANA_VERSION 4.1.1
ENV KIBANA_ARCH x64
ENV KIBANA_PORT 5601
ENV KIBANA_HOME /opt/kibana
ENV KIBANA_CONF /opt/kibana/config

LABEL Image.name="Kibana" Image.version=$KIBANA_VERSION
LABEL Exposed.ports=$KIBANA_PORT Arch=$KIBANA_ARCH
LABEL Base.OS="Ubuntu" BASE.OS.Version=$UBUNTU_VERSION

RUN groupadd -r kibana && useradd -r -g kibana kibana
#Kibana Installation
RUN curl --progress-bar --remote-name --insecure https://download.elastic.co/kibana/kibana/kibana-$KIBANA_VERSION-linux-$KIBANA_ARCH.tar.gz && \
   tar xf kibana-$KIBANA_VERSION-linux-$KIBANA_ARCH.tar.gz && \
   rm kibana-$KIBANA_VERSION-linux-$KIBANA_ARCH.tar.gz && \
   mv kibana-$KIBANA_VERSION-linux-$KIBANA_ARCH $KIBANA_HOME

ENV PATH $KIBANA_HOME/bin:$PATH
COPY docker-entrypoint.sh /
COPY config $KIBANA_CONF

RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE $KIBANA_PORT

CMD ["kibana"]